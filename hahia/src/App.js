import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
 import { BrowserRouter, Routes, Route } from "react-router-dom";
import  Home  from "./components/Home/Home";
import NavbarComponent from './components/Navbar/Navbar';
import Auth from './components/Authentication/Auth';


function App() {
  return (
    <div>
      <NavbarComponent />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/auth/*" element={<Auth />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
