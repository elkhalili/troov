import { React } from "react";

function Steper() {
  return (
    <section className="contain-steper">
    <section>
      <div className="step"></div>
    </section>
    <section className="stepers">
      <div className="step-contain">
        <div className="number-contain">
          <p>1</p>
        </div>
        <div className="text-contain">
          <h3>Déclarez un objet perdu ou un objet trouvé</h3>
          <p>Remplissez le formulaire de déclaration en fournissant tous les détails possibles(lieu de perte, type d'objet, description)</p>
        </div>
      </div>
      <div className="step-contain">
        <div className="number-contain">
          <p>2</p>
        </div>
        <div className="text-contain-reverse">
          <h3>Contactez ceux qui trouvent votre objet </h3>
          <p>Contactez facilement avec ceux qui retrouvent vos objets perdus</p>
        </div>
      </div>
      <div className="step-contain">
        <div className="number-contain">
          <p>3</p>
        </div>
        <div className="text-contain">
          <h3>Récupérez-le</h3>
          <p>Rencontrez la personne qui a trouvé l'objet que vous avez perdu et récupérez-le</p>
        </div>
      </div>
    </section>
  </section>
  
  );
}

export default Steper;
