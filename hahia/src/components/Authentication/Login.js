import React from 'react'
import { useForm } from 'react-hook-form';

function Login() {
    const { register, handleSubmit } = useForm();
      const onSubmit =  (data, e) => {
        console.log("data0", data)
        e.preventDefault(); 
            
      };
return (
    <div className="Auth-form-container">
    <form className="Auth-form d-flex justify-content-center" onSubmit={(data, e) => handleSubmit(onSubmit)(data, e)}>
      <div className="Auth-form-content m-5 w-25">
        <h3 className="Auth-form-title d-flex justify-content-center">Sign In</h3>
        <div className="form-group mt-3">
          <label>Email</label>
          <input
          {...register("login")}
            type="text"
            className="form-control mt-1"
            placeholder="Email"
          />
        </div>
        <div className="form-group mt-3">
          <label>Password</label>
          <input
          {...register("password")}
            type="password"
            className="form-control mt-1"
            placeholder="password"
          />
        </div>
        <div className="d-grid gap-2 mt-3" style={{ display: "flex" }}>
          <button type="submit" className="btn btn-primary">
            Connexion
          </button>
          <a href="/auth/signup">sign up</a>
        </div>      
        <p className="text-center mt-2">
          Forgot <a href="/auth/forgot-password">Mot de Passe</a>
        </p>
      </div>
    </form>
  </div>
)
}

export default Login
